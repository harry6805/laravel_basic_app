<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePatientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $patient = $this->all();
        if(isset($patient['id'])){
            return [
                'name' => 'required|max:255',
                'email' => 'required|email|unique:surgeons,email,'. $patient['id'],
                'gender' => 'required',
                'phone' => 'required|phone_number',
                'age' => 'required',
                'surgeon' => 'required',
            ];
        }
        return [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:surgeons',
            'gender' => 'required',
            'phone' => 'required|phone_number',
            'age' => 'required',
            'surgeon' => 'required',
        ];
    }
}
