<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSurgeonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $surgeon = $this->all();
        if(isset($surgeon['id'])){
            return [
                'name' => 'required|max:255',
                'email' => 'required|email|unique:surgeons,email,'. $surgeon['id'],
            ];
        }
        return [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:surgeons',
        ];
    }
}
