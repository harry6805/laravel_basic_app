<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\StoreUserRequest;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('users/index');
    }
    public function create()
    {
        return view('users/create');
    }
    public function store(StoreUserRequest $request)
    {
        $input = $request->all();
        $user = new User;
        if($user->create($input)){
            $success = 'user is created';
        }else {
            $fail = "user is not created";
        }
        return view('users/index',compact('fail','success'));
    }
}
