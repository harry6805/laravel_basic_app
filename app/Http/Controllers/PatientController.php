<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Patient;
use App\Surgeon;
use App\Http\Requests\StorePatientRequest;

class PatientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->surgeons_name = array();
        $surgeons = Surgeon::all()->toArray();
        foreach($surgeons as $index => $value){
            $this->surgeons_name[$value['id']] = $value['name'];
        }
    }
    public function index()
    {
        $patients = Patient::latest()->get();
        return view('patients/index',compact('patients'));
    }

    public function show($id)
    {
        $patient = Patient::findOrFail($id);
        $surgeon = $patient->belongsToSurgeon;
        if($surgeon){
            $patient->surgeon = $surgeon->name;
        } else {
            $patient->surgeon = "";
        }
        return view('patients/show',compact('patient'));
    }
    public function create()
    {
        $surgeons = $this->surgeons_name;
        return view('patients/create',compact('surgeons'));
    }
    public function store(StorePatientRequest $request)
    {
        $input = $request->all();
        $patient = new Patient;
        $patient->create($input);
        return redirect('/patients');
    }
    public function edit($id)
    {
        $surgeons = $this->surgeons_name;
        $patient = Patient::findOrFail($id);
        return view('patients/edit',compact('patient','surgeons'));
    }
    public function update(StorePatientRequest $request)
    {
        $input = $request->all();
        $patient = new Patient;
        if($patient->update_patient($input)){
            return redirect('/patients');
        }
        else{
           $fail = "update patient failed";
           return view("patients/edit",compact('fail')); 
        }
    }
    public function delete($id)
    {
        $patient = Patient::findOrFail($id);
        if($patient->delete()){
            return redirect('/patients');
        }
            $fail = "delete patient failed";
            return view('patients/show',compact('fail'));
    }
}
