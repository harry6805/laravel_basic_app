<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Surgeon;
use App\Http\Requests\StoreSurgeonRequest;

class SurgeonController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $surgeons = Surgeon::latest()->get();
        return view('surgeons/index',compact('surgeons'));
    }
    public function show($id)
    {
        $surgeon = Surgeon::findOrFail($id);
        return view('surgeons/show',compact('surgeon'));
    }
    public function create()
    {
        return view('surgeons/create');
    }
    public function store(StoreSurgeonRequest $request)
    {
        $input = $request->all();
        $surgeon = new Surgeon;
        $surgeon->create($input);
        return redirect('/surgeons');
    }


    public function edit($id)
    {
        $surgeon = Surgeon::findOrFail($id);
        return view('surgeons/edit',compact('surgeon'));
    }
    public function update(StoreSurgeonRequest $request)
    {
        $input = $request->all();
        $surgeon = new Surgeon;
        if($surgeon->update_surgeon($input)){
            return redirect('/surgeons');
        }
        else{
           $fail = "update surgeon failed";
           return view("surgeons/edit",compact('fail')); 
        }
    }
    public function delete($id)
    {
        $surgeon = Surgeon::findOrFail($id);
        if($surgeon->delete()){
            return redirect('/surgeons');
        }
            $fail = "delete surgeon failed";
            return view('surgeons/show',compact('fail'));
    }
}
