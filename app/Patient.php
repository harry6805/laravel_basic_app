<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

class Patient extends Model
{
    protected $patients_columns;

    public function __construct()
    {
        $this->patients_columns = Schema::getColumnListing('patients');
    }
    public function belongsToSurgeon()
    {
        return $this->belongsTo('App\Surgeon','surgeon');
    }

    public function create($input)
    {
        foreach($input as $key => $value){
            if(in_array($key,$this->patients_columns)){
                $this->{$key} = $value;
            }
        }
        $this->save();

    }
    public function update_patient(array $input)
    {
        $input_model = $this::findOrFail($input['id']);
        $input_array = $input_model->toArray();
        foreach($input as $key => $value){
            if(isset($input_array[$key])){
                $input_model->{$key} = $value;
            }
        }
        //$input_model['updated_at'] = Carbon::now();
        return $input_model->save();
    }
}
