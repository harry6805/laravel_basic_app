<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $users_columns;

    public function __construct()
    {
        $this->users_columns = Schema::getColumnListing('users');
    }
    public function create($input)
    {
        foreach($input as $key => $value){
            if(in_array($key,$this->users_columns)){
                $exists = $this->where($key,'=',$value);
                if($exists == $value){
                    return false;
                }
                if($key == "password"){
                    $value = bcrypt($value);
                }
                $this->{$key} = $value;
            }
        }
        $this->save();
        return true;

    }
}
