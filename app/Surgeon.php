<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Surgeon extends Model
{
    public function hasPatients()
    {
         return $this->hasMany('App\Patient','surgeon');
    }

    public function create($input)
    {
        $this->name = $input['name'];
        $this->email = $input['email'];
        $this->created_at = Carbon::now();
        $this->save();
    }
    public function update_surgeon(array $input)
    {
        $input_model = $this::findOrFail($input['id']);
        $input_array = $input_model->toArray();
        foreach($input as $key => $value){
            if(isset($input_array[$key])){
                $input_model->{$key} = $value;
            }
        }
        //$input_model['updated_at'] = Carbon::now();
        return $input_model->save();
    }

}
