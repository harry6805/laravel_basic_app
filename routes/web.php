<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index');
/* Users */
Route::get('/users','UserController@index');
Route::get('/users/create','UserController@create');
Route::post('/users/store','UserController@store');
/* Patients */
Route::get('/patients','PatientController@index');
Route::get('/patients/create','PatientController@create');
Route::post('/patients/store','PatientController@store');
Route::get('/patients/edit/{id}','PatientController@edit');
Route::post('/patients/update','PatientController@update');
Route::get('/patients/delete/{id}','PatientController@delete');
Route::get('/patients/{id}','PatientController@show');
/* Surgeons */
Route::get('/surgeons','SurgeonController@index');
Route::get('/surgeons/create','SurgeonController@create');
Route::post('/surgeons/store','SurgeonController@store');
Route::get('/surgeons/edit/{id}','SurgeonController@edit');
Route::post('/surgeons/update','SurgeonController@update');
Route::get('/surgeons/delete/{id}','SurgeonController@delete');
Route::get('/surgeons/{id}','SurgeonController@show');
