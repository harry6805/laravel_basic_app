@extends('layouts.app')
@section('content')
    <button onclick="myGoto()" class="btn btn-success">Add Users</button>
    <script language="javascript">
        function myGoto(){
            location.href="{{ url('/users/create') }}"
        }
    </script>
    <br>
    <br>
    @if(isset($success))
        <p>{{ $success }}</p>
    @endif
    @if(isset($fail))
        <p class="alert alert-danger">{{ $fail }}</p>
    @endif
@endsection
