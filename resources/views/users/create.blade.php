@extends('layouts.app')
@section('content')
    <h1>Add User</h1>
    <br>

    {!! Form::open(['action'=>'UserController@store','method'=>'post']) !!}
        <div class="form-group">
            {!! Form::label('name','Name:') !!}
            {!! Form::text('name',null,['class'=>'form-control']) !!}
        </div>
     
        <div class="form-group">
            {!! Form::label('email','Email:') !!}
            {!! Form::text('email',null,['class'=>'form-control']) !!}
        </div>
     
        <div class="form-group">
            {!! Form::label('password','Password:') !!} <br>
            {!! Form::password('password',null,['class'=>'form-control']) !!} <br>
            {!! Form::label('password_confirmation','Password(retry):') !!}  <br>
            {!! Form::password('password_confirmation',null,['class'=>'form-control']) !!}
        </div>
     
        <div class="form-group">
            {!! Form::submit('Add User',['class'=>'btn btn-success form-control']) !!}
        </div>
    {!! Form::close() !!}

    @if($errors->any())
            <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
    @endif
@endsection
