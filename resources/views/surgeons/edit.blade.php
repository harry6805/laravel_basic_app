@extends('layouts.app')
@section('content')

    @if(isset($fail))
        <p class="alert alert-danger">{{ $$fail }}</p>
    @endif
    <h1>Edit Surgeon</h1>
    <br>

    {!! Form::open(['action'=>'SurgeonController@update','method'=>'post']) !!}
        <div class="form-group">
            {!! Form::label('name','Name:') !!}
            {!! Form::text('name',$surgeon->name,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::hidden('id', $surgeon->id) !!}
        </div>
        <div class="form-group">
            {!! Form::label('email','Email:') !!}
            {!! Form::text('email',$surgeon->email,['class'=>'form-control']) !!}
        </div>
     
        <div class="form-group">
            {!! Form::submit('Update Surgeon',['class'=>'btn btn-success form-control']) !!}
        </div>
    {!! Form::close() !!}

    @if($errors->any())
            <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
    @endif
@endsection
