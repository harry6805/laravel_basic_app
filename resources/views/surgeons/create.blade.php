@extends('layouts.app')
@section('content')
    <h1>Add Surgeon</h1>
    <br>

    {!! Form::open(['action'=>'SurgeonController@store','method'=>'post']) !!}
        <div class="form-group">
            {!! Form::label('name','Name:') !!}
            {!! Form::text('name',null,['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('email','Email:') !!}
            {!! Form::text('email',null,['class'=>'form-control']) !!}
        </div>
     
        <div class="form-group">
            {!! Form::submit('Add Surgeon',['class'=>'btn btn-success form-control']) !!}
        </div>
    {!! Form::close() !!}

    @if($errors->any())
            <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
    @endif
@endsection
