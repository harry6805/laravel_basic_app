@extends('layouts/app')
@section('content')

    @if(isset($fail))
        <p class="alert alert-danger">{{ $fail }}</p>
    @endif
    <surgeon class="format-image group">
        <h2 class="post-title pad">
            Name: {{ $surgeon->name }}
        </h2>

        <div class="post-inner">
            <div class="post-content pad">
                <div class="entry custome">
                    <p>Email: {{ $surgeon->email }}</p>
                    <p>Created At: {{ $surgeon->created_at }}</p>
                    <p>Updated At: {{ $surgeon->updated_at }}</p>
                </div>
            </div>
        </div>
    </surgeon>
    <br>
    <br>

    <button onclick="myGoto()" class="btn btn-success">Edit Surgeon</button>
    <button onclick="myConfirm()" class="btn btn-success">Delete Surgeon</button>

    <script language="javascript">
        function myGoto(){
            location.href="{{ url('surgeons/edit') . '/' . $surgeon->id }}"
        }

        function myConfirm(){
            if(confirm("Are you sure?")){
                location.href="{{ url('surgeons/delete') . '/' . $surgeon->id }}"
            }
        }
    </script>
@endsection
