@extends('layouts.app')
@section('content')
    <h1>Add Patient</h1>
    <br>

    {!! Form::open(['action'=>'PatientController@store','method'=>'post']) !!}
        <div class="form-group">
            {!! Form::label('name','Name:') !!}
            {!! Form::text('name',null,['class'=>'form-control']) !!}
        </div>
     
        <div class="form-group">
            {!! Form::label('gender','Gender:') !!}<br>
            Male:   {!! Form::radio('gender','1',['class'=>'form-control','checked'=>'']) !!}<br>
            Female: {!! Form::radio('gender','0',['class'=>'form-control','checked'=>'']) !!}
        </div>
     
        <div class="form-group">
            {!! Form::label('phone','Phone:') !!}
            {!! Form::text('phone',null,['class'=>'form-control']) !!}
        </div>
     
        <div class="form-group">
            {!! Form::label('email','Email:') !!}
            {!! Form::text('email',null,['class'=>'form-control']) !!}
        </div>
     
        <div class="form-group">
            {!! Form::label('age','Age:') !!}
            {!! Form::text('age',null,['class'=>'form-control']) !!}
        </div>
     
        <div class="form-group">
            {!! Form::label('surgeon','Surgeon:') !!}
            {!! Form::select('surgeon',$surgeons) !!}
        </div>
     
        <div class="form-group">
            {!! Form::submit('Add Patient',['class'=>'btn btn-success form-control']) !!}
        </div>
    {!! Form::close() !!}

    @if($errors->any())
            <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
    @endif
@endsection
