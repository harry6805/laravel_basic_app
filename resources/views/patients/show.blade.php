@extends('layouts/app')
@section('content')

    @if(isset($fail))
        <p class="alert alert-danger">{{ $fail }}</p>
    @endif
    <patient class="format-image group">
        <h2 class="post-title pad">
            Name: {{ $patient->name }}
        </h2>

        <div class="post-inner">
            <div class="post-content pad">
                <div class="entry custome">
                    @if($patient->gender == 1)
                        <p>Gender: Male</p>
                    @else
                        <p>Gender: Female</p>
                    @endif 
                    <p>Phone: {{ $patient->phone }}</p>
                    <p>Email: {{ $patient->email }}</p>
                    <p>Age: {{ $patient->age }}</p>
                    <p>Surgeon: {{ $patient->surgeon }}</p>
                    <p>Created At: {{ $patient->created_at }}</p>
                    <p>Updated At: {{ $patient->updated_at }}</p>
                </div>
            </div>
        </div>
    </patient>
    <br>
    <br>

    <button onclick="myGoto()" class="btn btn-success">Edit Patient</button>
    <button onclick="myConfirm()" class="btn btn-success">Delete Patient</button>

    <script language="javascript">
        function myGoto(){
            location.href="{{ url('patients/edit') . '/' . $patient->id }}"
        }

        function myConfirm(){
            if(confirm("Are you sure?")){
                location.href="{{ url('patients/delete') . '/' . $patient->id }}"
            }
        }
    </script>
@endsection
