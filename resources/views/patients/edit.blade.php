@extends('layouts.app')
@section('content')

    @if(isset($fail))
        <p class="alert alert-danger">{{ $$fail }}</p>
    @endif
    <h1>Edit Patient</h1>
    <br>

    {!! Form::open(['action'=>'PatientController@update','method'=>'post']) !!}
        <div class="form-group">
            {!! Form::label('name','Name:') !!}
            {!! Form::text('name',$patient->name,['class'=>'form-control']) !!}
        </div>
     
        <div class="form-group">
            {!! Form::label('gender','Gender:') !!}<br>
            @if($patient->gender == 1)
                Male: {!! Form::radio('gender', '1', true) !!}
                Female: {!! Form::radio('gender', '0', false) !!}
            @else
                Male: {!! Form::radio('gender', '1', false) !!}
                Female: {!! Form::radio('gender', '0', true) !!}
            @endif
        </div>
     
        <div class="form-group">
            {!! Form::hidden('id', $patient->id) !!}
        </div>
     
        <div class="form-group">
            {!! Form::label('phone','Phone:') !!}
            {!! Form::text('phone',$patient->phone,['class'=>'form-control']) !!}
        </div>
     
        <div class="form-group">
            {!! Form::label('email','Email:') !!}
            {!! Form::text('email',$patient->email,['class'=>'form-control']) !!}
        </div>
     
        <div class="form-group">
            {!! Form::label('age','Age:') !!}
            {!! Form::text('age',$patient->age,['class'=>'form-control']) !!}
        </div>
     
        <div class="form-group">
            {!! Form::label('surgeon','Surgeon:') !!}
            {!! Form::select('surgeon',$surgeons) !!}
        </div>
     
        <div class="form-group">
            {!! Form::submit('Update Patient',['class'=>'btn btn-success form-control']) !!}
        </div>
    {!! Form::close() !!}

    @if($errors->any())
            <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
    @endif
@endsection
