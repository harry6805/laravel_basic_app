@extends('layouts.app')
@section('content')
    <button onclick="myGoto()" class="btn btn-success">Add Patient</button>
    <br>
    <br>
    <script language="javascript">
        function myGoto(){
            location.href="{{ url("patients/create") }}"
        }
    </script>
    @foreach($patients as $patient)
        <article class="format-image group">
        <h2 class="post-title pad">
            <a href="/patients/{{ $patient->id }}">Name: {{ $patient->name }}</a></h1>
        </h2>
        <div class="post-inner">
            <div class="post-deco">
                <div class="hex hex-small">
                    <div class="hex-inner"><i class="fa"></i></div>
                    <div class="corner-1"></div>
                    <div class="corner-2"></div>
                </div>
            </div>
            <div class="post-content pad">
                <div class="entry custome">
                    Phone: {{ $patient->phone }}
                </div>
            </div>
        </div>
    @endforeach
@endsection
