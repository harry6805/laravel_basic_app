<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        $userMode = new App\User;

        $userMode->create([
            'name'    => 'test',
            'email' => 'test@pulseinfoframe.com',
            'password' => bcrypt("111111"),
            'created_at' => date("Y-m-d h:i:s")
        ]);
    }
}
