<?php $__env->startSection('content'); ?>

    <?php if(isset($fail)): ?>
        <p class="alert alert-danger"><?php echo e($$fail); ?></p>
    <?php endif; ?>
    <h1>Edit Patient</h1>
    <br>

    <?php echo Form::open(['action'=>'PatientController@update','method'=>'post']); ?>

        <div class="form-group">
            <?php echo Form::label('name','Name:'); ?>

            <?php echo Form::text('name',$patient->name,['class'=>'form-control']); ?>

        </div>
     
        <div class="form-group">
            <?php echo Form::label('gender','Gender:'); ?><br>
            <?php if($patient->gender == 1): ?>
                Male: <?php echo Form::radio('gender', '1', true); ?>

                Female: <?php echo Form::radio('gender', '0', false); ?>

            <?php else: ?>
                Male: <?php echo Form::radio('gender', '1', false); ?>

                Female: <?php echo Form::radio('gender', '0', true); ?>

            <?php endif; ?>
        </div>
     
        <div class="form-group">
            <?php echo Form::hidden('id', $patient->id); ?>

        </div>
     
        <div class="form-group">
            <?php echo Form::label('phone','Phone:'); ?>

            <?php echo Form::text('phone',$patient->phone,['class'=>'form-control']); ?>

        </div>
     
        <div class="form-group">
            <?php echo Form::label('email','Email:'); ?>

            <?php echo Form::text('email',$patient->email,['class'=>'form-control']); ?>

        </div>
     
        <div class="form-group">
            <?php echo Form::label('age','Age:'); ?>

            <?php echo Form::text('age',$patient->age,['class'=>'form-control']); ?>

        </div>
     
        <div class="form-group">
            <?php echo Form::label('surgeon','Surgeon:'); ?>

            <?php echo Form::select('surgeon',$surgeons); ?>

        </div>
     
        <div class="form-group">
            <?php echo Form::submit('Update Patient',['class'=>'btn btn-success form-control']); ?>

        </div>
    <?php echo Form::close(); ?>


    <?php if($errors->any()): ?>
            <ul class="alert alert-danger">
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>