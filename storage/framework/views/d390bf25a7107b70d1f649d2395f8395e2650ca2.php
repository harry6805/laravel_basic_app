<?php $__env->startSection('content'); ?>

    <?php if(isset($fail)): ?>
        <p class="alert alert-danger"><?php echo e($fail); ?></p>
    <?php endif; ?>
    <surgeon class="format-image group">
        <h2 class="post-title pad">
            Name: <?php echo e($surgeon->name); ?>

        </h2>

        <div class="post-inner">
            <div class="post-content pad">
                <div class="entry custome">
                    <p>Email: <?php echo e($surgeon->email); ?></p>
                    <p>Created At: <?php echo e($surgeon->created_at); ?></p>
                    <p>Updated At: <?php echo e($surgeon->updated_at); ?></p>
                </div>
            </div>
        </div>
    </surgeon>
    <br>
    <br>

    <button onclick="myGoto()" class="btn btn-success">Edit Surgeon</button>
    <button onclick="myConfirm()" class="btn btn-success">Delete Surgeon</button>

    <script language="javascript">
        function myGoto(){
            location.href="<?php echo e(url('surgeons/edit') . '/' . $surgeon->id); ?>"
        }

        function myConfirm(){
            if(confirm("Are you sure?")){
                location.href="<?php echo e(url('surgeons/delete') . '/' . $surgeon->id); ?>"
            }
        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts/app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>