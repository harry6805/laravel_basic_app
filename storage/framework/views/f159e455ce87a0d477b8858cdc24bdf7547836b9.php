<?php $__env->startSection('content'); ?>
<div id="title" style="text-align: center;">
    <h1>Add Users</h1>
</div>

<div id="nav" style="text-align: center;text-size: 50;">
  <form enctype="multipart/form-data" method="POST" action="<?php echo e(url('useradmin/add')); ?>">
    <?php echo csrf_field(); ?>

    <fieldset>

      <legend>Add User</legend>
      <label for="name">Name:</label>
      <input type="text" id="name" name="name" value="" /><br />
      <label for="email">Email:</label>
      <input type="text" id="email" name="email" value="" /><br />
      <label for="password">Password:</label>
      <input type="password" id="password" name="password" value="" /><br />
      <label for="password2">Password (retype):</label>
      <input type="password" id="password2" name="password2" value="" /><br />
    </fieldset>
    <input type="submit" value="Add" name="submit" />
  </form>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>