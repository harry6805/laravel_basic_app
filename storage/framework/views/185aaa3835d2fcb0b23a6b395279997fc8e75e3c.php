<?php $__env->startSection('content'); ?>

    <?php if(isset($fail)): ?>
        <p class="alert alert-danger"><?php echo e($$fail); ?></p>
    <?php endif; ?>
    <h1>Edit Surgeon</h1>
    <br>

    <?php echo Form::open(['action'=>'SurgeonController@update','method'=>'post']); ?>

        <div class="form-group">
            <?php echo Form::label('name','Name:'); ?>

            <?php echo Form::text('name',$surgeon->name,['class'=>'form-control']); ?>

        </div>
        <div class="form-group">
            <?php echo Form::hidden('id', $surgeon->id); ?>

        </div>
        <div class="form-group">
            <?php echo Form::label('email','Email:'); ?>

            <?php echo Form::text('email',$surgeon->email,['class'=>'form-control']); ?>

        </div>
     
        <div class="form-group">
            <?php echo Form::submit('Update Surgeon',['class'=>'btn btn-success form-control']); ?>

        </div>
    <?php echo Form::close(); ?>


    <?php if($errors->any()): ?>
            <ul class="alert alert-danger">
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>