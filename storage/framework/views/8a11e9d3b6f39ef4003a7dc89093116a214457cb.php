<?php $__env->startSection('content'); ?>

    <?php if(isset($fail)): ?>
        <p class="alert alert-danger"><?php echo e($fail); ?></p>
    <?php endif; ?>
    <patient class="format-image group">
        <h2 class="post-title pad">
            Name: <?php echo e($patient->name); ?>

        </h2>

        <div class="post-inner">
            <div class="post-content pad">
                <div class="entry custome">
                    <?php if($patient->gender == 1): ?>
                        <p>Gender: Male</p>
                    <?php else: ?>
                        <p>Gender: Female</p>
                    <?php endif; ?> 
                    <p>Phone: <?php echo e($patient->phone); ?></p>
                    <p>Email: <?php echo e($patient->email); ?></p>
                    <p>Age: <?php echo e($patient->age); ?></p>
                    <p>Surgeon: <?php echo e($patient->surgeon); ?></p>
                    <p>Created At: <?php echo e($patient->created_at); ?></p>
                    <p>Updated At: <?php echo e($patient->updated_at); ?></p>
                </div>
            </div>
        </div>
    </patient>
    <br>
    <br>

    <button onclick="myGoto()" class="btn btn-success">Edit Patient</button>
    <button onclick="myConfirm()" class="btn btn-success">Delete Patient</button>

    <script language="javascript">
        function myGoto(){
            location.href="<?php echo e(url('patients/edit') . '/' . $patient->id); ?>"
        }

        function myConfirm(){
            if(confirm("Are you sure?")){
                location.href="<?php echo e(url('patients/delete') . '/' . $patient->id); ?>"
            }
        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts/app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>