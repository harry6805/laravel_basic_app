<?php $__env->startSection('content'); ?>
    <h1>Add Patient</h1>
    <br>

    <?php echo Form::open(['action'=>'PatientController@store','method'=>'post']); ?>

        <div class="form-group">
            <?php echo Form::label('name','Name:'); ?>

            <?php echo Form::text('name',null,['class'=>'form-control']); ?>

        </div>
     
        <div class="form-group">
            <?php echo Form::label('gender','Gender:'); ?><br>
            Male:   <?php echo Form::radio('gender','1',['class'=>'form-control','checked'=>'']); ?><br>
            Female: <?php echo Form::radio('gender','0',['class'=>'form-control','checked'=>'']); ?>

        </div>
     
        <div class="form-group">
            <?php echo Form::label('phone','Phone:'); ?>

            <?php echo Form::text('phone',null,['class'=>'form-control']); ?>

        </div>
     
        <div class="form-group">
            <?php echo Form::label('email','Email:'); ?>

            <?php echo Form::text('email',null,['class'=>'form-control']); ?>

        </div>
     
        <div class="form-group">
            <?php echo Form::label('age','Age:'); ?>

            <?php echo Form::text('age',null,['class'=>'form-control']); ?>

        </div>
     
        <div class="form-group">
            <?php echo Form::label('surgeon','Surgeon:'); ?>

            <?php echo Form::select('surgeon',$surgeons); ?>

        </div>
     
        <div class="form-group">
            <?php echo Form::submit('Add Patient',['class'=>'btn btn-success form-control']); ?>

        </div>
    <?php echo Form::close(); ?>


    <?php if($errors->any()): ?>
            <ul class="alert alert-danger">
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>