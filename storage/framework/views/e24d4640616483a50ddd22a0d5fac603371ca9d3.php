<?php $__env->startSection('content'); ?>
    <button onclick="myGoto()" class="btn btn-success">Add Users</button>
    <script language="javascript">
        function myGoto(){
            location.href="<?php echo e(url('/users/create')); ?>"
        }
    </script>
    <br>
    <br>
    <?php if(isset($success)): ?>
        <p><?php echo e($success); ?></p>
    <?php endif; ?>
    <?php if(isset($fail)): ?>
        <p class="alert alert-danger"><?php echo e($fail); ?></p>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>