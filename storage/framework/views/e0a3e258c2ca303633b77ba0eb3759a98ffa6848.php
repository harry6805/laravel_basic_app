<?php $__env->startSection('content'); ?>
    <button onclick="myGoto()" class="btn btn-success">Add Surgeo</button>
    <br>
    <br>
    <script language="javascript">
        function myGoto(){
            location.href="<?php echo e(url('surgeons/create')); ?>"
        }
    </script>
    <?php $__currentLoopData = $surgeons; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $surgeon): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <article class="format-image group">
        <h2 class="post-title pad">
            <a href="/surgeons/<?php echo e($surgeon->id); ?>">Name: <?php echo e($surgeon->name); ?></a></h1>
        </h2>
        <div class="post-inner">
            <div class="post-deco">
                <div class="hex hex-small">
                    <div class="hex-inner"><i class="fa"></i></div>
                    <div class="corner-1"></div>
                    <div class="corner-2"></div>
                </div>
            </div>
            <div class="post-content pad">
                <div class="entry custome">
                    Phone: <?php echo e($surgeon->email); ?>

                </div>
            </div>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>